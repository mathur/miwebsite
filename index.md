---
title: First Jekyll Page
# this can be empty, but we need the 3 dashes
---

# Building Websites

## Description 

{{site.description}}

Welcome to {{page.title}}  
This is a banana {{site.banana}}  
This is about me : [ME ME ME](about)  
[Send me an email](mailto:{{site.email}})